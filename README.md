# Práctica 2: eSportsLS

eSportsLS es un proyecto (ficticio) creado por la universidad para organizar las competiciones del juego League of Legends. En primer lugar, se pretende organizar una liga entre los mejores equipos profesionales y, posteriormente, reunir a los mejores jugadores en sus respectivas selecciones para crear un torneo mundial.

## Requerimientos

Es imprescindible descargar Visual Studio 2019 y tener instalado .NET Core 3.0. Es un proyecto desarrollado en C# con las últimas versiones de dependencias.

## Instalación

Descargar el zip del proyecto. Abrir el archivo .sln en Visual Studio 2019.

## Usage

Abrir el archivo de las Propiedades del proyecto (Project > eSports Properties...).

Application arguments:
```bash
<json_teams> <json_champs> <json_comida>
```
Por ejemplo:
```bash
"datasetP18.json" "dataset_Champ.json" "dataset50.json"
```

Working directory: Seleccionar la carpeta donde se enuentran los JSON.

Hacer clic a Start.

## Autores
Kaye Ann Ignacio Jove - kayeann.ignacio
Nicole Marie Jimenez Burayag - nicolemarie.jimenez

PAED @ Febrero 2020

## License
[MIT](https://choosealicense.com/licenses/mit/)