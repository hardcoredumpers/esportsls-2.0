﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using eSportsLS_p2.Functionalities;
using Newtonsoft.Json;

namespace eSportsLS_p2.Model
{
    class DataManager
    {
        public struct Element
        {
            public String nationality;
            public String mainChamp;
            public String counterPick;
        }
        public ArrayList arrayList;
        public eSports[] teams;
        public Jugador[] jugadores;
        public Food[] comida;
        public Champion[] champs;
        public Backtracking bt;
        public BranchNBound bb;
        public Greedy gd;
        public ArrayList element;
        public Element[] jugadorEquipo;
        public Stopwatch stopwatch;
        public TimeSpan ts;

        public DataManager()
        {
            initializeMethods();
            stopwatch = new Stopwatch();
        }

        private void initializeMethods()
        {
            bt = new Backtracking();
            bb = new BranchNBound();
            gd = new Greedy();
        }

        public void readJSON(string jsonChamps, string jsonTeams, string jsonComida)
        {
            // Load teams
            using (StreamReader r = new StreamReader(jsonTeams))
            {
                string json = r.ReadToEnd();
                var esports = JsonConvert.DeserializeObject<List<eSports>>(json);
                arrayList = new ArrayList();

                foreach (var data in esports)
                {
                    arrayList.Add(data);
                }
                teams = arrayList.ToArray(typeof(eSports)) as eSports[];
            }

            // Create array of players
            ArrayList arrayListAux = new ArrayList();
            foreach (eSports e in teams)
            {
                foreach (Jugador j in e.players)
                {
                    j.name = char.ToUpper(j.name[0]) + j.name.Substring(1);
                    j.main_position = char.ToUpper(j.main_position[0]) + j.main_position.Substring(1);
                    arrayListAux.Add(j);
                }
            }
            jugadores = arrayListAux.ToArray(typeof(Jugador)) as Jugador[];

            // Load food
            using (StreamReader r = new StreamReader(jsonComida))
            {
                string json = r.ReadToEnd();
                var foods = JsonConvert.DeserializeObject<List<Food>>(json);
                arrayList = new ArrayList();

                foreach (var data in foods)
                {
                    arrayList.Add(data);
                }
                comida = arrayList.ToArray(typeof(Food)) as Food[];
            }

            // Load champions
            using (StreamReader r = new StreamReader(jsonChamps))
            {
                string json = r.ReadToEnd();
                var champions = JsonConvert.DeserializeObject<List<Champion>>(json);
                arrayList = new ArrayList();

                foreach (var data in champions)
                {
                    arrayList.Add(data);
                }
                champs = arrayList.ToArray(typeof(Champion)) as Champion[];
            }

            //Guardamos el equipo del jugador
            int h = 0;
            jugadorEquipo = new Element[jugadores.Length];   
            foreach (eSports aux in teams)
            {
                foreach(Jugador jAux in aux.players)
                {
                    bool encontrado = false;
                    for (int b = 0; b < champs.Length && !encontrado; b++)
                    {
                        if (jAux.main_champion[0].name.Equals(champs[b].name))
                        {
                            encontrado = true;
                            jAux.counterPick = champs[b].counter_picks;
                        }

                    }
                }
            }            
        }

        public void start(int option)
        {
            switch(option)
            {
                // Food
                case 1:
                    stopwatch.Start();
                    SolutionMenu solMenuGreedy = gd.GreedyImpl(comida);
                    SolutionMenu best;
                    int totalSolutionsBB = 0;
                    if (solMenuGreedy == null)
                    {
                        Console.WriteLine("Greedy no tiene solución.");
                        solMenuGreedy = new SolutionMenu();
                        solMenuGreedy.comida = new ArrayList();
                        solMenuGreedy.total_kilocalories = 0;
                        solMenuGreedy.total_fat = 0;
                        solMenuGreedy.consumed_fat = int.MaxValue;
                        best = bb.BranchAndBound(comida, comida[0], solMenuGreedy, ref totalSolutionsBB);
                    } else
                    {
                        best = bb.BranchAndBound(comida, comida[0], solMenuGreedy, ref totalSolutionsBB);
                    }
                    stopwatch.Stop();
                    Console.WriteLine("Best Menu: ");
                    best.printSolution();
                    best.showPlates();
                    ts = stopwatch.Elapsed;
                    Console.WriteLine("Time elapsed: " + ts);
                    stopwatch.Reset();
                    Console.WriteLine("Total solutions found: " + totalSolutionsBB);
                    break;

                // Teams
                case 2:
                    int g = 0;
                    SolutionFaseGrup solInicial = new SolutionFaseGrup(teams);
                    SolutionFaseGrup solFinal = new SolutionFaseGrup(teams);
                    //Inicialización
                    eSports teamES = null;
                    int cantGrups = 0;
                    bool notExists = true;
                    List<eSports> aux = new List<eSports>();
                    Group[] grups = new Group[teams.Length / 6];
                    solFinal.total_counters = 0;
                    for (int z = 0; z < grups.Length; z++)
                    {
                        solFinal.grups[z] = new Group();
                    }
                    for (int a = 0; a < grups.Length; a++)
                    {
                        int indexES = 0;
                        for (int m = 0; m < cantGrups; m++)
                        {
                            aux.AddRange(solFinal.grups[m].teams);
                        }
                        for (int b = 0; b < teams.Length && (solFinal.grups[a].teams.Count < 6); b++)
                        {
                            if (a > 0)
                            {
                               
                                if (aux.Contains(teams[b])) { notExists = false; }
                                else { notExists = true; }
                                if (notExists)
                                {
                                    if (teams[b].nationality.Equals("España") && !solFinal.grups[a].hasSpanishTeam)
                                    {
                                        solFinal.grups[a].teams.Add(teams[b]);
                                        solFinal.grups[a].hasSpanishTeam = true;
                                        indexES = b;
                                        teamES = teams[b];
                                    }
                                    if (!teams[b].nationality.Equals("España"))
                                    {
                                        solFinal.grups[a].teams.Add(teams[b]);
                                    }
                                }
                            }
                            else
                            {
                                if (teams[b].nationality.Equals("España") && !solFinal.grups[a].hasSpanishTeam)
                                    {
                                        solFinal.grups[a].teams.Add(teams[b]);
                                        solFinal.grups[a].hasSpanishTeam = true;
                                        indexES = b;
                                        teamES = teams[b];
                                    }
                                if (!teams[b].nationality.Equals("España"))
                                {
                                    solFinal.grups[a].teams.Add(teams[b]);
                                }

                            }
                        }
                        solFinal.total_counters += solFinal.grups[a].calcularCounterGrup(teamES, indexES, "suma");
                        cantGrups++;
                    }
                    solFinal.cantTeams = teams.Length;
                    stopwatch.Start();
                    SolutionFaseGrup bestTeam = bt.BacktrackingImpl(solInicial, ref solFinal, teams, g);
                    stopwatch.Stop();
                    ts = stopwatch.Elapsed;
                    Console.WriteLine("Time elapsed: " + ts);
                    stopwatch.Reset();
                    Console.WriteLine("\n\nBest solution:");
                    bestTeam.printSolution();
                    break;
            }
        }
    }
}
