﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Model
{
    class Jugador
    {
        public String name { get; set; }
        public String main_position { get; set; }
        public Legend[] main_champion { get; set; }

        public String counterPick { get; set; }
    }
}
