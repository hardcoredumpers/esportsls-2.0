﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Model
{
    class eSports
    {
        public String team { get; set; }
        public String nationality { get; set; }
        public float winrate { get; set; }
        public Jugador[] players { get; set; }
    }
}
