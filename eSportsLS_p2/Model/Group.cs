﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Model
{
    class Group
    {
        public List<eSports> teams { get; set; }
        public int num_counters { get; set; }
        public bool hasSpanishTeam { get; set; }

        public Group()
        {
            teams = new List<eSports>();
            num_counters = int.MaxValue;
            hasSpanishTeam = false;
        }

        public int calcularCounterGrup (eSports equipES, int indexES, String type)
        {
            num_counters = 0;
            for (int i = 0; i < teams.Count; i++)
            {
                if (i != indexES) 
                {
                    if (type.Equals("suma"))
                    {
                        num_counters += calculateCounterEquip(teams[i], equipES, type);
                    }
                    if (type.Equals("resta"))
                    {
                        num_counters -= calculateCounterEquip(teams[i], equipES, type);
                    }
                }
            }
            return num_counters;

        }

        private int calculateCounterEquip(eSports equip, eSports equipES, String type)
        {
            int counters = 0;
            for (int i = 0; i < equip.players.Length; i++)
            {
                for (int j = 0; j < equipES.players.Length; j++)
                {
                    //Miramos que para un equipo no español es counterpick a un equipo español;
                    if (equip.players[i].main_champion[0].name.Equals(equipES.players[j].counterPick))
                    {
                        if (type.Equals("suma"))
                        {
                            counters++;
                        }
                        if (type.Equals("resta"))
                        {
                            counters--;
                        }
                    }
                }
            }
            return counters;
        }
    }
}

    


