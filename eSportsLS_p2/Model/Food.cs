﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Model
{
    class Food : IComparable<Food>
    {
        public String name { get; set; }
        public int energetic_value { get; set; }
        public float fat { get; set; }

        public virtual int CompareTo(Food obj)
        {
            return (energetic_value.CompareTo(obj.energetic_value));
        }
    }
}
