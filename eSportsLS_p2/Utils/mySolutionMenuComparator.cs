﻿using eSportsLS_p2.Functionalities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Utils
{
    class mySolutionMenuComparator : IComparer
    {
        int IComparer.Compare(Object x, Object y)
        {
            SolutionMenu actual = (SolutionMenu)x;
            SolutionMenu nou = (SolutionMenu)y;

            if (actual.total_kilocalories < nou.total_kilocalories)
                return -1;
            else if (actual.total_kilocalories == nou.total_kilocalories)
            {
                if (actual.total_fat < nou.total_fat)
                {
                    return -1;
                } else if (actual.total_fat == nou.total_fat)
                {
                    return 0;
                } else
                {
                    return -1;
                }
            } else
            {
                return 1;
            }
               
        }
    }
}
