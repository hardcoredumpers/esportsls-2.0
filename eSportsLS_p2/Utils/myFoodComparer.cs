﻿using eSportsLS_p2.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Utils
{
    public class myFoodComparer : IComparer
    {

        int IComparer.Compare(Object x, Object y)
        {
            Food actual = (Food)x;
            Food nou = (Food)y;

            if (actual.energetic_value < nou.energetic_value)
                return -1;
            else if (actual.energetic_value == nou.energetic_value)
                return 0;
            else
                return 1;
        }

    }
}
