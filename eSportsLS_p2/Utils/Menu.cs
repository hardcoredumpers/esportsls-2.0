﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS_p2.Model
{
    class Menu
    {
        private static int MAX = 3;
        private static int MIN = 1;
        private int option;

        public void showMenu()
        {
            Console.WriteLine("Welcome to eSportsLS 2.0\n");
            Console.WriteLine("1. Menú más equilibrado\n2. Fase de grupos\n3. Exit\n\nOption: ");
        }

        public int getOption() { return option; }

        public bool validOption() { return option >= MIN && option <= MAX; }

        public void optionInput() { option = Convert.ToInt32(Console.ReadLine()); }

        public bool isExit() { return option == MAX; }
    }
}
