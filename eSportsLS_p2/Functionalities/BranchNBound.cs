﻿using eSportsLS_p2.Model;
using eSportsLS_p2.Utils;
using System;
using System.Collections;

namespace eSportsLS_p2.Functionalities
{
    class BranchNBound
    {
        /// <summary>
        /// Implementación del algoritmo de Branch and Bound para encontrar él menú más equilibrado dado un array de platos.
        /// </summary>
        /// <param name="comida">El array de platos</param>
        /// <param name="current">El plato actual</param>
        /// <param name="best">El mejor menú o el menú más equilibrado</param>
        /// <param name="totalSolutions">El número total de soluciones encontradas</param>
        /// <returns></returns>
        public SolutionMenu BranchAndBound(Food[] comida, Food current, SolutionMenu best, ref int totalSolutions)
        {
            PriorityQueue menu_queue = new PriorityQueue(new mySolutionMenuComparator());
            SolutionMenu solActual = new SolutionMenu();
            ArrayList options;

            // Inicialización
            solActual.comida = new ArrayList();
            solActual.comida.Add(current);
            solActual.total_kilocalories += current.energetic_value;
            solActual.total_fat += current.fat;
            solActual.calculateConsumedFat();

            // Añadir la nueva solución a la cola prioritaria
            menu_queue.Enqueue(solActual);

            while (menu_queue.Count != 0)
            {
                solActual = (SolutionMenu)menu_queue.Dequeue();

                options = expand(comida, solActual);

                foreach (SolutionMenu opt in options)
                {
                    if (isSolution(opt, best))
                    {
                        copySolutionMenu(opt, ref best);
                        best.printSolution();
                        totalSolutions++;
                    }
                    else
                    {
                        if (isPromising(opt, best))
                        {
                            menu_queue.Enqueue(opt);
                        }
                    }
                }
                
            }
            return best;
        }

        /// <summary>
        /// Copiar el contenido de una solución a otra solución
        /// </summary>
        /// <param name="origin">La solución origen</param>
        /// <param name="dest">La solución destino</param>
        private void copySolutionMenu(SolutionMenu origin, ref SolutionMenu dest)
        {
            dest.comida = copyContents(origin.comida);
            dest.consumed_fat = origin.consumed_fat;
            dest.total_fat = origin.total_fat;
            dest.total_kilocalories = origin.total_kilocalories;
        }

        /// <summary>
        /// Copia el contenido de un arraylist origen a un arraylist destino
        /// </summary>
        /// <param name="a">El arraylist que se quiere copiar </param>
        /// <returns></returns>
        private static ArrayList copyContents(ArrayList a)
        {
            ArrayList clone = (ArrayList)a.Clone();
            return clone;
        }

        /// <summary>
        /// Comprueba si la solución obtenida ya es una solución definitiva y final
        /// </summary>
        /// <param name="solActual">La solución que se evalúa</param>
        /// <param name="best">La mejor solución</param>
        /// <returns>Retorna true si ya és una solución. Si no, false.</returns>
        private bool isSolution(SolutionMenu solActual, SolutionMenu best)
        {
            bool ok = false;
            if (solActual.validFatConsumption() && solActual.comida.Count <= SolutionMenu.MAX_PLATS)
            {
                //if (isPromising(solActual, best))
                if (solActual.validKiloCalories() && solActual.total_kilocalories >= best.total_kilocalories && solActual.consumed_fat < best.consumed_fat)
                {
                    ok = true;
                }
            }

            return ok;
        }

        /// <summary>
        /// Comprueba si una solución obtenida podría ser una solución definitiva
        /// </summary>
        /// <param name="opt">La solución nueva que se evalúa</param>
        /// <param name="best">La mejor solución en la que se basa la comparación</param>
        /// <returns>Retorna true si puede ser una solución definitiva. Si no, false.</returns>
        private bool isPromising(SolutionMenu opt, SolutionMenu best)
        {
            bool ok = false;

            if (opt.validKiloCalories())
            {
                if (opt.total_kilocalories >= best.total_kilocalories)
                {
                    ok = true;
                }
                else
                {
                    if (opt.consumed_fat < best.consumed_fat)
                    {
                        ok = true;
                    }
                }
            }

            return ok;
        }

        /// <summary>
        /// Expandir a partir del plato y sumar las calorias y grasas a cada plato
        /// </summary>
        /// <param name="comida">El array de los platos</param>
        /// <param name="solActual">La solución actual de la cual queremos expandir y extraer opciones</param>
        /// <returns>Una lista de las opciones generadas a partir de los platos</returns>
        private ArrayList expand(Food[] comida, SolutionMenu solution)
        {
            ArrayList options = new ArrayList();

            for (int i = 0; i < comida.Length; i++)
            {
                if (!plateInMenu(comida[i], solution))
                {
                    SolutionMenu newSol = new SolutionMenu();
                    copySolutionMenu(solution, ref newSol);
                    newSol.total_kilocalories += comida[i].energetic_value;
                    newSol.total_fat += comida[i].fat;
                    newSol.calculateConsumedFat();
                    newSol.comida.Add(comida[i]);
                    options.Add(newSol);
                }
            }

            return options;
        }

        /// <summary>
        /// Comprueba si ya existe el plato en el menu de la solución
        /// </summary>
        /// <param name="food">El plato</param>
        /// <param name="solution">La solución que tiene el menú</param>
        /// <returns>Retorna true si ya existe. Si no, false. Albert was here:).</returns>
        private bool plateInMenu(Food food, SolutionMenu solution)
        {
            return solution.comida.Contains(food);
        }

    }
}
