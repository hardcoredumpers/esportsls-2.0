﻿using eSportsLS_p2.Model;
using System;
using System.Collections;

namespace eSportsLS_p2.Functionalities
{
    class SolutionMenu
    {
        public ArrayList comida { get; set; }
        public int total_kilocalories { get; set; }
        public float total_fat { get; set; }
        public float consumed_fat { get; set; }

        public const int MAX_PLATS = 5;
        public const int MIN_KILOCALO = 1000;
        public const int MAX_KILOCALO = 3000;
        public const int MIN_FAT = 20;
        public const int MAX_FAT = 35;


        public void calculateConsumedFat()
        {
            consumed_fat = (total_fat / total_kilocalories) * 100;
        }

        public bool validKiloCalories()
        {
            return this.total_kilocalories >= MIN_KILOCALO && this.total_kilocalories <= MAX_KILOCALO;
        }

        public bool validFatConsumption()
        {
            return this.consumed_fat >= MIN_FAT && this.consumed_fat <= MAX_FAT;
        }

        public void printSolution()
        {
            Console.WriteLine("Total KC: " + this.total_kilocalories + " Consumed fat: " + this.consumed_fat + " Total plates: " + this.comida.Count);

        }

       public void showPlates()
        {
            foreach (Food f in comida)
            {
                Console.WriteLine("\t- " + f.name);
            }
        }
    }
}
