﻿using eSportsLS_p2.Model;
using System;
using System.Collections;

namespace eSportsLS_p2.Functionalities
{
    class Backtracking
    {
        public SolutionFaseGrup BacktrackingImpl(SolutionFaseGrup solActual, ref SolutionFaseGrup best, eSports[] teams, int g)
        {
            if (isSolution(solActual, teams))
            {
                best = minCounters(solActual, best, teams);
                best.printSolution();
            } 
            else
            {
                for (int i = 0; i < teams.Length; i++)
                {
                    Group aux = (Group) solActual.grups[g];
                    if (!teamExists(solActual, teams[i], g) && isFactible(solActual, teams[i], g))
                    {
                        aux.teams.Add(teams[i]);
                        marcatge(solActual, g, teams[i]);
                        if (solActual.total_counters < best.total_counters) 
                        { 
                            
                            if (isGrupComplete(solActual, g))
                            {
                                best = BacktrackingImpl(solActual, ref best, teams, (g + 1));
                            }
                            else
                            {
                                best = BacktrackingImpl(solActual, ref best, teams, g);
                            }
                        }
                        aux.teams.Remove(teams[i]);
                        desmarcatge(solActual, g, teams[i]);
                    }
                   
                }
            }
            return best;
        }

        private bool isFactible(SolutionFaseGrup solActual, eSports team, int g)
        {
            Group grp = (Group)solActual.grups[g];
            if (g > 0)
            {
                //Miramos si el team que queremos añadir ya está en otro grupo o no.
                foreach (Group aux in solActual.grups)
                {
                    if (aux.teams.Count > 0 && aux.teams.Contains(team))
                    {
                        return false;
                    }
                }
                //Si no está ya colocado en otro grupo, verificamos si es de nacionalidad
                //española y si lo es, que no tengamos ya uno en el grupo
                if (!grp.hasSpanishTeam && team.nationality.Equals("España"))
                {
                    return true;
                }
            }
            if (grp.teams.Count > 0)
            {
                //Si el equipo es español y ya tenemos uno, descartamos
                //Si el equipo es español y no tenemos uno, añadimos
                //Si el equipo no es español, añadimos
                if ((team.nationality.Equals("España") && !grp.hasSpanishTeam))
                {
                    return true;
                }
                if (!team.nationality.Equals("España")) { return true; }
            }
            else
            {
                return true;
            }
           
            return false;
        }

        private bool teamExists (SolutionFaseGrup sol, eSports teamName, int g)
        {
            Group grp = (Group)sol.grups[g];
            foreach (Object aux in grp.teams)
            {
                if (aux.Equals(teamName))
                {
                    return true;
                }
            }
            return false;
        }

        private SolutionFaseGrup minCounters(SolutionFaseGrup solActual, SolutionFaseGrup best, eSports[] teams)
        {
            if(solActual.total_counters < best.total_counters) {
                copySolutionFaseGrup(solActual, ref best, teams);
            }
            return best;

        }

        private void copySolutionFaseGrup (SolutionFaseGrup actual, ref SolutionFaseGrup best, eSports[] teams)
        {
            best.cantTeams = actual.cantTeams;
            SolutionFaseGrup tmp = new SolutionFaseGrup(teams);
            tmp.cantTeams = actual.cantTeams;
            tmp.total_counters = actual.total_counters;
            
            for (int i = 0; i < actual.grups.Length; i++)
            {
                foreach (eSports tms in actual.grups[i].teams)
                {
                    tmp.grups[i].teams.Add(tms);
                }
                tmp.grups[i].hasSpanishTeam = actual.grups[i].hasSpanishTeam;
                tmp.grups[i].num_counters = actual.grups[i].num_counters;
            }
            best.grups = tmp.grups;
            best.total_counters = actual.total_counters;
        }
        private bool isSolution(SolutionFaseGrup solActual, eSports[] teams)
        {
            //Si ya se han distribuido los equipos en los grupos
            return solActual.cantTeams == teams.Length;
        }

        private bool isGrupComplete(SolutionFaseGrup solActual, int g)
        {
            Group aux = (Group)solActual.grups[g];
            return aux.teams.Count == 6;
        }
        private void marcatge (SolutionFaseGrup solActual, int g, eSports team)
        {
            Group aux = (Group)solActual.grups[g];
            if (!aux.hasSpanishTeam && team.nationality.Equals("España"))
            {
                aux.hasSpanishTeam = true;

            }
            solActual.cantTeams++;
            solActual.calcularTotalCounter(g, "suma");
        }
        private void desmarcatge(SolutionFaseGrup solActual, int g, eSports team)
        {
            Group aux = (Group)solActual.grups[g];
            if (team.nationality.Equals("España"))
            {
                aux.hasSpanishTeam = false;

            }
            solActual.cantTeams--;
            solActual.calcularTotalCounter(g, "resta");
        }
    }
}
