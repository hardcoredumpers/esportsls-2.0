﻿using eSportsLS_p2.Model;
using eSportsLS_p2.Utils;
using System.Collections;

namespace eSportsLS_p2.Functionalities
{
    /// <summary>
    /// Greedy Implementation for eSportsLS functionalities.
    /// </summary>
    class Greedy
    {
        /// <summary>
        /// Implementación de Greedy para generar un menú equilibrado.
        /// </summary>
        /// <returns>
        /// El menú quilibrado con un máximo de 5 platos. Puede no dar una solución.
        /// </returns>
        public SolutionMenu GreedyImpl(Food[] comida)
        {
            bool stop = false;
            int i = 0;

            // Inicializar solución
            SolutionMenu solActual = new SolutionMenu();
            solActual.comida = new ArrayList();
            solActual.total_kilocalories = 0;
            solActual.total_fat = 0;
            solActual.consumed_fat = 0;

            // Ordenamos por las kilocalories
            ArrayList sortedComida = new ArrayList(comida);
            sortedComida.Sort(new myFoodComparer());
            sortedComida.Reverse();

            while (!stop && i < SolutionMenu.MAX_PLATS)
            {
                Food food = (Food) sortedComida[i];

                if (reachedMaxKiloCal(solActual.total_kilocalories + food.energetic_value))
                {
                    stop = true;

                } else
                {
                    solActual.total_kilocalories += food.energetic_value;
                    solActual.total_fat += food.fat;
                    solActual.calculateConsumedFat();
                    solActual.comida.Add(food);
                }
                i++;
            }

            // Si no es una solución válida, retorna null
            if (!isSolution(solActual)) {
                solActual = null;
            }
            return solActual;
        }

        private bool reachedMaxKiloCal(int kilocalories)
        {
            return kilocalories > SolutionMenu.MAX_KILOCALO;
        }

        private bool isSolution(SolutionMenu sol)
        {
            return sol.validKiloCalories() && sol.validFatConsumption();
        }

    }
}
