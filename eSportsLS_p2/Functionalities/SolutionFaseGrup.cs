﻿using eSportsLS_p2.Model;
using System;
using System.Collections;
using System.Text;

namespace eSportsLS_p2.Functionalities
{
    class SolutionFaseGrup
    {
        public Group[] grups { get; set; }
        public int total_counters { get; set; }
        public int cantTeams { get; set; }

        public SolutionFaseGrup(eSports[] teams)
        {
            grups = new Group[teams.Length / 6];
            for (int i = 0; i < grups.Length; i++)
            {
                grups[i] = new Group();
            }
            total_counters = int.MaxValue;
            cantTeams = 0;
        }

        private eSports getEquipES(int g, ref int i)
        {
            for (int h = 0; h < grups[g].teams.Count; h++)
            {
                if (grups[g].teams[h].nationality.Equals("España"))
                {
                    i = h;
                    return grups[g].teams[h];
                }
            }
            return null;
        }

        //Calcula el counter para un grupo en concreto
        public void calcularTotalCounter(int g, String type)
        {
            //Cogemos el equipo español del grupo i guardamos su indice
            int i = 0;
            eSports equipES = getEquipES(g, ref i);
            if (equipES != null)
            {
                //Retorna el total de counters per aquest grup
                if (g == 0)
                {
                    total_counters = grups[g].calcularCounterGrup(equipES, i, type);
                }
                else
                {
                    total_counters += grups[g].calcularCounterGrup(equipES, i, type);
                }
            }
        }
        public void printSolution()
        {
            for (int i = 0; i < grups.Length; i++)
            {
                Console.WriteLine("\nGrup " + (i + 1));
                for (int j = 0; j < grups[i].teams.Count; j++)
                {
                    Console.WriteLine("E" + (j + 1));
                    Console.WriteLine("Nationalitat: " + grups[i].teams[j].nationality + " - " + grups[i].teams[j].team);
                    if (grups[i].teams[j].nationality.Equals("España"))
                    {
                        for (int h = 0; h < grups[i].teams[j].players.Length; h++)
                        {
                            Console.Write(grups[i].teams[j].players[h].counterPick + " | ");
                        }
                    }
                    else
                    {
                        for (int h = 0; h < grups[i].teams[j].players.Length; h++)
                        {
                            Console.Write(grups[i].teams[j].players[h].main_champion[0].name + " | ");
                        }
                    }
                    Console.WriteLine("");

                }
            }
            Console.WriteLine("\n\nCounters: " + this.total_counters);
        }
    }
        
}
