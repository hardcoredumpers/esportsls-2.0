﻿using System;
using eSportsLS_p2.Model;

namespace eSportsLS_p2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filenameTeams = args[0];
            string filenameChamps = args[1];
            string filenameFood = args[2];
            Menu menu = new Menu();
            DataManager program = new DataManager();
            program.readJSON(filenameChamps, filenameTeams, filenameFood);

            do
            {
                menu.showMenu();
                menu.optionInput();
                //Si la opción es valida y no es la de salir, entramos
                if (menu.validOption() && !menu.isExit())
                {
                    program.start(menu.getOption());
                }
            } while (!menu.validOption() || !menu.isExit());
        }
    }
}
